﻿using System;
using System.Collections.Generic;
using static System.Console;

namespace Module3_2
{
    public static class Program
    {
        static void Main(string[] args)
        {
            #region T4_Check
            var T4 = new Task4();
            int userInputT4;
            int[] aRR4;
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine("Task 4\nЧисла Фибоначчи");
            Console.ForegroundColor = ConsoleColor.Yellow;
            Write("Введите длину ряда ");
            Console.ForegroundColor = ConsoleColor.Magenta;
            userInputT4 = Convert.ToInt32(ReadLine());
            Write("\n");

            aRR4 = T4.GetFibonacciSequence(userInputT4);
            Console.ForegroundColor = ConsoleColor.White;
            Console.WriteLine("Мы получили следующий массив из чисел Фибоначчи:");

            Console.ForegroundColor = ConsoleColor.Green;
            Arr2ConsoleOut(aRR4);
            Console.ForegroundColor = ConsoleColor.White;
            Write("\n---------------------------------------------------------------------\n\n");
        #endregion

        #region T5_Check
            var T5 = new Task5();
            int userInputT5;
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine("Task 5\nРеверсируем строку");
            Console.ForegroundColor = ConsoleColor.Yellow;
            Write("Введите число для реверса ");

            userInputT5 = Convert.ToInt32(ReadLine());

            Write("\n");
            Console.ForegroundColor = ConsoleColor.White;
            Write("OK. Задом наперёд ");
            Console.ForegroundColor = ConsoleColor.Red;
            Write(userInputT5);
            Console.ForegroundColor = ConsoleColor.White;
            Write(" будет ");
            Console.ForegroundColor = ConsoleColor.Green;
            userInputT5 = T5.ReverseNumber(userInputT5);
            Write(userInputT5);
            Console.ForegroundColor = ConsoleColor.White;
            Write("\n---------------------------------------------------------------------\n\n");
            #endregion

            #region T6_Check
            int userInputT6;
            int[] Arrr;
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine("Task 6\nМеняем знак в массиве");
            Console.ForegroundColor = ConsoleColor.Yellow;
            Write("Введите длину массива ");

            Console.ForegroundColor = ConsoleColor.Magenta;
            userInputT6 = Convert.ToInt32(ReadLine());
            Write("\n");

            var T6 = new Task6();
            Console.ForegroundColor = ConsoleColor.White;

            Arrr = T6.GenerateArray(userInputT6);
            WriteLine("OK. Мы сгенерировали массив");

            Console.ForegroundColor = ConsoleColor.Blue;
            Arr2ConsoleOut(Arrr);
            WriteLine("");

            Console.ForegroundColor = ConsoleColor.Yellow;
            WriteLine("Теперь сменим знак в массиве\n");

            Console.ForegroundColor = ConsoleColor.White;
            Arrr = T6.UpdateElementToOppositeSign(Arrr);
            WriteLine("Теперь массив выглядит так:");

            Console.ForegroundColor = ConsoleColor.Green;
            Arr2ConsoleOut(Arrr);
            Console.ForegroundColor = ConsoleColor.White;
            Write("\n---------------------------------------------------------------------\n\n");

            Console.ResetColor();

            static void Arr2ConsoleOut(int[] arrSource)
            {

                if (arrSource == null)
                {
                    return;
                }

                for (int i = 0; i < arrSource.Length; ++i)
                {
                    Console.Write(arrSource[i] + " ");
                }
            }
            ReadKey();
        #endregion

        #region Task7_Check
            Console.Clear();
            int userInputT7;
            var T7 = new Task7();
            int[] aRR7;
            List<int> L7;
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine("Task 7\nИщем большие постстоящие значения в массиве");
            Console.ForegroundColor = ConsoleColor.Yellow;
            Write("Введите длину массива ");

            Console.ForegroundColor = ConsoleColor.Magenta;
            userInputT7 = Convert.ToInt32(ReadLine());
            Write("\n");

            aRR7 = T7.GenerateArrayMinMax(userInputT7, -20, 20);
            Console.ForegroundColor = ConsoleColor.White;
            Console.WriteLine("\nМы получили следующий массив:");

            Arr2ConsoleOut(aRR7);

            L7 = new List<int>(T7.FindElementGreaterThenPrevious(aRR7));

            Console.WriteLine("\nМы получили следующий список больших постстоящих:");

            list2ConsoleOut();

            void list2ConsoleOut()
            {
                WriteLine("\n");

                Console.ForegroundColor = ConsoleColor.Green;
                foreach (int val in L7)
                {
                    Write(val + " ");
                }
                WriteLine("\n");

            }
            ReadKey();
            Console.Clear();
            #endregion

            
            #region Task8_Check
            int userInputT8;
            int[,] arr8Check;
            Console.ForegroundColor = ConsoleColor.Red;
            Console.Write("Таска 8\nЗаполняем двумерный массив по спирали\n");
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.Write("Введите размерность массива N*N ");
            Console.ForegroundColor = ConsoleColor.Magenta;
            userInputT8 = Convert.ToInt32(ReadLine());
            Write("\n");

            Console.ForegroundColor = ConsoleColor.Green;
            var T8 = new Task8();
            arr8Check = T8.FillArrayInSpiral(userInputT8);

            for (int i = 0; i < arr8Check.GetLength(0); i++)
            {
                for (int j = 0; j < arr8Check.GetLength(1); j++)
                {
                    Console.Write(arr8Check[i, j] + "\t");
                }
                Console.WriteLine();
            }

            #endregion
            Console.ForegroundColor = ConsoleColor.DarkGray;
        }
    }

    public class Task4
    {
        /// <summary>
        /// Use this method to parse and validate user input
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public bool TryParseNaturalNumber(string input, out int result)
        {
            try
            {
                result = int.Parse(input);
                if (result < 0)
                {
                    return false;
                }
                return true;
            }
            catch
            {
                result = 0;
                return false;
            }
        }

        public int[] GetFibonacciSequence(int n)
        {
            int[] aRR;

            if (n <= 0)
            {
                aRR = new int[0];
            }
            else if (n == 1)
            {
                aRR = new int[n];
                aRR[0] = 0;
            }
            else if (n == 2)
            {
                aRR = new int[n];
                aRR[0] = 0;
                aRR[1] = 1;
            }
            else
            {
                aRR = new int[n];
                aRR[0] = 0;
                aRR[1] = 1;
                for (int i = 2; i < n; i++)
                {
                    aRR[i] = aRR[i - 1] + aRR[i - 2];
                }
            }
            return aRR;
        }
    }

    public class Task5
    {
        public int ReverseNumber(int sourceNumber)
        {
            string retconverted;
            char[] inputarray;
            string sign = "";
            string result;

            if (sourceNumber < 0)
            {
                sign = "-";
                sourceNumber = -sourceNumber;
            }

            retconverted = Convert.ToString(sourceNumber);
            inputarray = retconverted.ToCharArray();

            Array.Reverse(inputarray);
            result = new string(inputarray);
            result = sign + result;

            return Convert.ToInt32(result);
        }
    }
    public class Task6
    {
        ///6)	Дан массив, содержащий положительные и отрицательные числа. 
        /// <summary>
        /// Use this method to generate array. It shouldn't throws exception.
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public int[] GenerateArray(int size)
        {
            int[] arrGen;
            if (size <= 0)
            {
                arrGen = new int[0];
                return arrGen;
            }

            Random RND = new Random();
            arrGen = new int[size];
            for (int i = 0; i < arrGen.Length; ++i)
            {
                arrGen[i] = RND.Next(-2147483648, 2147483647);
            }
            return arrGen;
        }

        public int[] UpdateElementToOppositeSign(int[] source)
        {
            for (int i = 0; i < source.Length; ++i)
            {
                source[i] = -source[i];
            }
            return source;
        }
    }

    public class Task7
    {
        /// <summary>
        /// Use this method to generate array. It shouldn't throws exception.
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public int[] GenerateArray(int size)
        {
            int[] arrGen;
            if (size <= 0)
            {
                arrGen = new int[0];
                return arrGen;
            }

            Random RND = new Random();
            arrGen = new int[size];
            for (int i = 0; i < arrGen.Length; ++i)
            {
                arrGen[i] = RND.Next(-2147483648, 2147483647);
            }
            return arrGen;
        }

        public int[] GenerateArrayMinMax(int size, int min, int max)
        {
            int[] arrGen;
            if (size <= 0)
            {
                arrGen = new int[0];
                return arrGen;
            }

            Random RND = new Random();
            arrGen = new int[size];
            for (int i = 0; i < arrGen.Length; ++i)
            {
                arrGen[i] = RND.Next(min, max);
            }
            return arrGen;
        }

        public List<int> FindElementGreaterThenPrevious(int[] source)
        {
            List<int> numbersList = new List<int>();

            for (int i = 0; i < source.Length - 1; ++i)
            {
                if (source[i] < source[i + 1])
                {
                    numbersList.Add(source[i+1]);
                }                
            }
            return numbersList;
        }
    }

    public class Task8
    {
        public int[,] FillArrayInSpiral(int size)
        {
            int[,] aRR8;

            if (size <= 0)
            {
                aRR8 = new int[0, 0];
                return aRR8;
            }

            int val = 1;
            int x = -1;
            int y = 0;
            aRR8 = new int[size, size];
            for (int i = 1; i < 2 * size; i++)
            {
                for (int j = 0; j < size - i / 2; j++)
                {
                    y += (i % 4 - 1) % 2;
                    x -= (i % 4 - 2) % 2;
                    aRR8[y, x ] = val++;
                }
            }
            return aRR8;
        }
    }
}
